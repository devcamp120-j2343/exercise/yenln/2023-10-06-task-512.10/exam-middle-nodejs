//B1: Import Thư viện express
//import { Express } from "express";
const express = require("express");

//Import thư viện mongoose
var mongoose = require('mongoose');

const path = require('path');

var cors = require('cors')

//b2 khởi tạo app express
const app = new express();

//b3: khai báo cỗng để chạy api
const port = 8000;

const router = express.Router();


mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Vaccination")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleError(error));

const userRouter = require("./app/routes/UserRouter");
const contactRouter = require("./app/routes/ContactRouter");

//cấu hình để sử dụng json
app.use(cors());
app.use("/static", express.static('./static/'));
app.use(express.json());

app.use(express.static(__dirname + "/views"));


//Middleware
//Middleware console log ra thời gian hiện tại 
app.use((req, res, next) => {
    console.log("Thời gian hiện tại:", new Date());

    next();
})

app.get("/", (request, response) => {
    console.log(__dirname);

    response.render('')

    response.sendFile(path.join(__dirname, "/views/vaccination.html"))
})

app.router("/admin/users", (request, response) => {
    response.sendFile(path.join(__dirname, "/views/administration.html"))
})


// app.get('/users/:id', function (req, res, next) {
//     res.json({msg: 'This is CORS-enabled for all origins!'})
//   })

app.use("/users", userRouter);
app.use("/contacts", contactRouter);

//b4: star app
app.listen(port, () => {
    console.log(`CORS-enabled web server listening on port ${port}`)
    console.log(`app listening on port ${port}`);
})
