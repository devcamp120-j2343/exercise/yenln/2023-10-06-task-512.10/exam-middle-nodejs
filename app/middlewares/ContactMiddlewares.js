const getAllContactsMiddlewares = (req, res, next) => {
    console.log("Get all Contacts Middleware");

    next();
}

const createContactsMiddlewares = (req, res, next) => {
    console.log("Create Contacts Middleware");

    next();
}

const getDetailContactsMiddlewares = (req, res, next) => {
    console.log("Get detail Contact Middleware");

    next();
}

const updateContactsMiddlewares = (req, res, next) => {
    console.log("Update Contacts Middleware");

    next();
}

const deleteContactsMiddlewares = (req, res, next) => {
    console.log("Delete Contacts Middleware");

    next();
}

module.exports = {
    getAllContactsMiddlewares,
    createContactsMiddlewares, 
    getDetailContactsMiddlewares,
    updateContactsMiddlewares,
    deleteContactsMiddlewares
}