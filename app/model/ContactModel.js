//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const contactSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    email: {
        type: String,
        required: true,
    },
})

//B4: biên dịch schema thành model
module.exports = mongoose.model("contact", contactSchema)