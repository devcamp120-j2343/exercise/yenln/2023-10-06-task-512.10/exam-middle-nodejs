const mongoose = require("mongoose");

const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const contactModel = require("../model/ContactModel")
const contactRouter = require("../routes/ContactRouter");

const createContact = async (req, res) => {
    //Thao tác với CSDL
    const {
        email
    } = req.body;

    if (!email || !emailRegexp.test(email)) {
        return res.status(400).json({
            message: "email khong hop le",
        });
    }

    const newContact = {
        _id: new mongoose.Types.ObjectId(),
        email
    }

    const result = await contactModel.create(newContact);

    try {
        return res.status(201).json({
            status: "Create new contact sucessfully",
            data : result
        })
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getAllContact = async (req, res) => {
    const result = await contactModel.find();
    try {
        if (result && result.length > 0) {
            return res.status(201).json({
                status: "Get all contacts sucessfully",
                result
            })
        }
        else {
            return res.status(404).json({
                status: "Not found any contact",
                result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getContactById = async (req, res) => {
    var contactId = req.params.contactId;

    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "contactId is invalid!"
        })
    }

    const result = await contactModel.findById(contactId);

    try {
        if (!result) {
            return res.status(404).json({
                status: "Not found detail of this contact",
                data: result
            })
        }
        else {
            return res.status(201).json({
                status: `Get contact ${contactId} sucessfully`,
                data: result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }


}

const updateContactById = async (req, res) => {
    //B1: thu thập dữ liệu
    const contactId = req.params.contactId;

    const {
        email
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "contactId is invalid!"
        })
    }

    if (!email || !emailRegexp.test(email)) {
        return res.status(400).json({
            message: "email khong hop le",
        });
    }

    //B3: thực thi model
    try {
        let updatedContact = {

        }

        if (email) {
            updatedContact.email = email;
        }

        const result = await contactModel.findByIdAndUpdate(
            contactId,
            updatedContact
        );

        if (result) {
            return res.status(200).json({
                status: `Update contact ${contactId} sucessfully`,
                data: updatedContact
            })
        } else {
            return res.status(404).json({
                status: "Not found any contact"
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteContactById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var contactId = req.params.contactId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "contactId is invalid!"
        })
    }

    try {
        const deletedcontact = await contactModel.findByIdAndDelete(contactId);

        if (deletedcontact) {
            return res.status(200).json({
                status: `Delete contact ${contactId} sucessfully`,
                data: deletedcontact
            })
        } else {
            return res.status(404).json({
                status: "Not found any contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createContact,
    getAllContact,
    getContactById,
    updateContactById,
    deleteContactById
}
