//import { Express } from "express";
const express = require("express");

const contactMiddleware = require("../middlewares/ContactMiddlewares");

const contactController = require("../controllers/contactController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
router.post("/", contactMiddleware.createContactsMiddlewares, contactController.createContact)
 
router.get("/", contactMiddleware.getAllContactsMiddlewares, contactController.getAllContact)

router.get("/:contactId",contactMiddleware.getDetailContactsMiddlewares, contactController.getContactById)

router.put("/:contactId",contactMiddleware.updateContactsMiddlewares, contactController.updateContactById)

router.delete("/:contactId",contactMiddleware.deleteContactsMiddlewares, contactController.deleteContactById)

module.exports = router;